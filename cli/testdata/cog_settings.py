import cogment as _cog
from types import SimpleNamespace

import data_pb2
import delta

_plane_class = _cog.ActorClass(
    name='plane',
    config_type=None,
    action_space=data_pb2.Human_PlaneAction,
    observation_space=data_pb2.Observation,
    observation_delta=data_pb2.ObservationDelta,
    observation_delta_apply_fn=delta.apply_delta,
    feedback_space=None
)

_ai_drone_class = _cog.ActorClass(
    name='ai_drone',
    config_type=data_pb2.DroneConfig,
    action_space=data_pb2.Ai_DroneAction,
    observation_space=data_pb2.Observation,
    observation_delta=data_pb2.ObservationDelta,
    observation_delta_apply_fn=delta.apply_delta,
    feedback_space=None
)

actor_classes = _cog.actor_class.ActorClassList(
    _plane_class,
    _ai_drone_class,
)


environment = SimpleNamespace(
    config_type=data_pb2.EnvConfig,
)

trial = SimpleNamespace(
    config_type=data_pb2.TrialConfig,
)
