package deployment

import (
	"encoding/json"
	"errors"
	"github.com/go-resty/resty/v2"
	"github.com/spf13/viper"
)

func PlatformClient(verbose bool) (*resty.Client, error) {
	baseURL := viper.GetString("default.url")
	token := viper.GetString("default.token")

	if baseURL == "" {
		return nil, errors.New("API URL is not defined, maybe try `cogment configure`")
	}

	client := resty.New()
	client.SetHostURL(baseURL)
	client.SetHeader("Authorization", "Token "+token)
	//client.SetAuthToken(token)
	client.SetDebug(verbose)

	// Registering global Error object structure for JSON/XML request
	//client.SetError(Error{}) // or resty.SetError(Error{})

	return client, nil
}

func ResponseFormat(i interface{}) ([]byte, error) {
	return json.Marshal(i)
}
