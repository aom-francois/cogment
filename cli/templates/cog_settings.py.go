package templates

const COG_SETTINGS_PY = `
import cogment as _cog
from types import SimpleNamespace

{{range .Import.Proto -}}
import {{.}}
{{end -}}
{{- range .Import.Python -}}
import {{.}}
{{end}}


{{- range .ActorClasses}}
_{{.Id}}_class = _cog.ActorClass(
    id='{{.Id}}',
    config_type={{if .ConfigType}}{{.ConfigType}}{{else}}None{{end}},
    action_space={{.Action.Space}},
	{{- with .Observation}}
    observation_space={{.Space}},
    observation_delta={{if .Delta}}{{.Delta}}{{else}}{{.Space}}{{end}},
    observation_delta_apply_fn={{if .DeltaApplyFn}}{{.DeltaApplyFn.Python}}{{else}}_cog.delta_encoding._apply_delta_replace{{end}},
    {{end -}}    
    feedback_space=None
)
{{end}}


actor_classes = _cog.actor_class.ActorClassList(
{{- range .ActorClasses}}
    _{{.Id}}_class,
{{- end}}
)

environment = SimpleNamespace(
    config_type={{if .Environment}}{{if .Environment.ConfigType}}{{.Environment.ConfigType}}{{else}}None{{end}}{{else}}None{{end}},
)

trial = SimpleNamespace(
    config_type={{if .Trial}}{{if .Trial.ConfigType}}{{.Trial.ConfigType}}{{else}}None{{end}}{{else}}None{{end}},
)
`
