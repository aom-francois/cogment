/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"bufio"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"io"
	"log"
	"os"
	"strings"
)

type Origin struct {
	Token string
	Url   string
}

const defaultApiUrl = "https://platform-gateway.cogsaas.com"

// configureCmd represents the configure command
var configureCmd = &cobra.Command{
	Use:   "configure",
	Short: "Configure Cogment CLI options",
	Hidden: true,
	Run: func(cmd *cobra.Command, args []string) {
		if err := runConfigureCmd(os.Stdin); err != nil {
			log.Fatalln(err)
		}

		fmt.Printf("Config file has been written to %s\n", cfgFile)

	},
}

func createOriginFromReader(stdin io.Reader) *Origin {
	reader := bufio.NewReader(stdin)
	o := Origin{Url: defaultApiUrl}

	fmt.Print("API token: ")
	token, _ := reader.ReadString('\n')
	o.Token = strings.TrimSuffix(token, "\n")

	return &o
}

func runConfigureCmd(stdin io.Reader) error {
	o := createOriginFromReader(stdin)

	viper.Set("default.token", o.Token)
	viper.Set("default.url", o.Url)

	if err := viper.WriteConfigAs(cfgFile); err != nil {
		return err
	}

	return nil
}

func init() {
	rootCmd.AddCommand(configureCmd)
}
