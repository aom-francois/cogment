#ifndef AOM_ORCHESTRATOR_TESTS_GRPC_MOCK_UTILS_H
#define AOM_ORCHESTRATOR_TESTS_GRPC_MOCK_UTILS_H

#include "aom/completion.h"
#include "gmock/gmock.h"
#include "grpcpp/support/async_unary_call.h"
#include "gtest/gtest.h"

#define ALLOW_ASYNC_REPLY_MOCKING(T)                                  \
  namespace std {                                                     \
  template <>                                                         \
  class default_delete<grpc::ClientAsyncResponseReaderInterface<T>> { \
   public:                                                            \
    void operator()(void* p) { delete (T*)p; }                        \
  };                                                                  \
  }                                                                   \
  // end of macro

template <typename R>
class FakeAsyncGrpcResponse
    : public grpc::ClientAsyncResponseReaderInterface<R> {
 public:
  FakeAsyncGrpcResponse(grpc::Status s, R r) : s_(s), r_(std::move(r)) {}

  ~FakeAsyncGrpcResponse() { EXPECT_TRUE(finished); }

  bool finished = false;

  grpc::Status s_;
  R r_;

  void StartCall() override {}

  void ReadInitialMetadata(void* tag) override {
    assert(false);  // TODO: if needed
  }

  void Finish(R* msg, grpc::Status* status, void* tag) override {
    finished = true;
    *msg = r_;
    *status = s_;
    if (tag) {
      auto as_completion = reinterpret_cast<aom::Completion*>(tag);
      as_completion->exec();
    }
  }
};

#endif