#include "orchestrator_fixture.h"
#include "aom/datalog/grpc_exporter.h"
#include "mock_datalog.h"

using namespace std::chrono_literals;

using testing::_;
using testing::Return;
using testing::ByMove;

cogment::EnvUpdateReply blank_env_reply(const std::string& obs_data = "") {
  cogment::EnvUpdateReply result;
  auto observation = result.mutable_observation_set()->add_observations();
  observation->set_snapshot(false);
  observation->set_content(obs_data);

  result.mutable_observation_set()->add_actors_map(0);
  result.mutable_observation_set()->add_actors_map(0);

  return result;
}

TEST_F(Orchestrator_test, datalog_validation) {
  auto trial = start_trial();

  cogment::TrialActionRequest req;
  req.set_trial_id(trial.trial_id());

  for (int i = 0; i < 100; ++i) {
    EXPECT_CALL(env, Update(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future(blank_env_reply(std::to_string(i))))));

    if( i != 0 ) {
      EXPECT_CALL(*agent, Reward(_,_))
        .Times(1)
        .WillOnce(Return(ByMove(ready_future<cogment::AgentRewardReply>())));
    }
      
    EXPECT_CALL(*agent, Decide(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future<cogment::AgentDecideReply>())));

    auto rep = orchestrator->Action(req);
    rep.get();   
  }

  end_trial(trial);

  // 100 actions, 100 logs
  EXPECT_EQ(datalog.size(), 100);

  EXPECT_EQ(datalog.at(0).observations().actors_map_size(), 2);
  EXPECT_EQ(datalog.at(0).observations().observations(0).content(), "init");

  for(int i = 1 ; i < 100; ++i) {
    EXPECT_EQ(datalog.at(i).observations().actors_map_size(), 2);
    EXPECT_EQ(datalog.at(i).observations().observations(0).content(), std::to_string(i-1));
  }
}

TEST_F(Orchestrator_test, datalog_contains_uid) {
  auto trial = start_trial("xyz");

  cogment::TrialActionRequest req;
  req.set_trial_id(trial.trial_id());


  EXPECT_CALL(env, Update(_,_))
    .Times(1)
    .WillOnce(Return(ByMove(ready_future(blank_env_reply()))));
    
  EXPECT_CALL(*agent, Decide(_,_))
    .Times(1)
    .WillOnce(Return(ByMove(ready_future<cogment::AgentDecideReply>())));

  auto rep = orchestrator->Action(req);

  end_trial(trial);

  EXPECT_EQ(datalog.size(), 1);
  EXPECT_EQ(datalog.at(0).user_id(), "xyz");
}

/*
TEST_F(Orchestrator_test, grpc_datalog) {
  aom::Mock_datalog datalog;
  aom::Grpc_datalog_exporter_base exporter;

  exporter.set_stub(&datalog);
  auto trial = exporter.begin_trial(uuids::uuid());

  cogment::DatalogSample sample;
  sample.set_trial_id("abc");

  {
    EXPECT_CALL(datalog, Log(_))
        .Times(1)
        .WillOnce(Return(ByMove(ready_future<cogment::LogReply>())));
    trial->add_sample(sample);
  }

  {
    EXPECT_CALL(datalog, Log(_))
        .Times(1)
        .WillOnce(Return(ByMove(ready_future<cogment::LogReply>())));
    trial->add_sample(sample);
  }

  {
    EXPECT_CALL(datalog, Log(_))
        .Times(1)
        .WillOnce(Return(ByMove(ready_future<cogment::LogReply>())));
    trial->add_sample(sample);
  }
}
*/