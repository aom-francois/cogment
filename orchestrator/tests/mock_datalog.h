#ifndef AOM_ORCHESTRATOR_TEST_MOCK_DATALOG_H
#define AOM_ORCHESTRATOR_TEST_MOCK_DATALOG_H

#include "gmock/gmock.h"
#include "cogment/api/data.egrpc.pb.h"

namespace aom {
  class Mock_datalog : public cogment::LogExporter::Stub_interface {
    public:
      virtual ~Mock_datalog() {}
      MOCK_METHOD1(Log, std::tuple<::easy_grpc::Stream_promise<::cogment::DatalogSample>, ::easy_grpc::Future<::cogment::LogReply>>(::easy_grpc::client::Call_options));

  };
}

#endif
