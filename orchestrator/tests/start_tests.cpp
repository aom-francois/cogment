
#include "orchestrator_fixture.h"

using namespace std::chrono_literals;

using testing::_;
using testing::Return;
using testing::ByMove;

TEST_F(Orchestrator_test, create_and_delete) {
  // Just double-check that the test fixture is behaving correctly
}


TEST_F(Orchestrator_test, start_success) {
    auto trial = start_trial();
    EXPECT_NE("", trial.trial_id());

    end_trial(trial);
}

TEST_F(Orchestrator_test_no_human, start_success) {
    //auto trial = start_trial();
    //EXPECT_NE("", trial.trial_id());
}

TEST_F(Orchestrator_test, env_failure_propagation) {
    EXPECT_CALL(env, Start(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(failed_future<cogment::EnvStartReply>())));

    ::cogment::TrialStartRequest req;
    auto rep = orchestrator->Start(req);

    EXPECT_THROW(rep.get(), std::runtime_error);
}

TEST_F(Orchestrator_test, agent_failure_propagation) {
    EXPECT_CALL(env, Start(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future<cogment::EnvStartReply>())));

    EXPECT_CALL(*agent, Start(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(failed_future<cogment::AgentStartReply>())));

    // There should be no end if start fails

    ::cogment::TrialStartRequest req;
    auto rep = orchestrator->Start(req);

    EXPECT_THROW(rep.get(), std::runtime_error);
}


TEST_F(Orchestrator_test, agent_decision_failure) {

    cogment::EnvStartReply env_start_rep;
    //One single absolute observation that goes to both actors...

    auto observation = env_start_rep.mutable_observation_set()->add_observations();
    observation->set_snapshot(true);
    observation->set_content("");

    env_start_rep.mutable_observation_set()->add_actors_map(0);
    env_start_rep.mutable_observation_set()->add_actors_map(0);

    EXPECT_CALL(env, Start(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future(env_start_rep))));

    EXPECT_CALL(*agent, Start(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future<cogment::AgentStartReply>())));

    // If the agent fails to make a decision, the trial should still begin.
    EXPECT_CALL(*agent, Decide(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(failed_future<cogment::AgentDecideReply>())));


    EXPECT_CALL(*agent, End(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future<cogment::AgentEndReply>())));
    
    ::cogment::TrialStartRequest req;
    auto rep = orchestrator->Start(req);

    EXPECT_NE("", rep.get().trial_id());
}
