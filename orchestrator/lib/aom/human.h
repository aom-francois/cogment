#ifndef AOM_ORCHESTRATOR_HUMAN_H
#define AOM_ORCHESTRATOR_HUMAN_H

#include "aom/actor.h"

namespace aom {

class Human : public Actor {
public:
  Human(std::string tid);
  ~Human();

  Future<void> init() override;
  void terminate() override;

  bool is_human() override {return true;}
  
  void dispatch_reward(int tick_id, const ::cogment::Reward& reward) override {}
  Future<cogment::Action> request_decision(cogment::Observation&& obs) override;
  ::easy_grpc::Future<::cogment::TrialActionReply> user_acted(
    cogment::TrialActionRequest req
  ) override ;

private:
  Promise<cogment::Action> human_action_promise_;
  Promise<::cogment::TrialActionReply> human_observation_promise_;
};
}  // namespace aom
#endif