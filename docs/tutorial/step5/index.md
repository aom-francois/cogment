# Step 5 : Dataset and Results


## Dump a local .log

The orchestrator can easily dump a datalog, with the results of your trials.

Outputting and viewing those results will be what we tackle in this Step 5 ; however, for the time being, we are actively working on making this process as easy and straightforward as possible.

We will update this section very soon.

