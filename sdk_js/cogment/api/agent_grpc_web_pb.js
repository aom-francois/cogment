/**
 * @fileoverview gRPC-Web generated client stub for cogment
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var cogment_api_common_pb = require('../../cogment/api/common_pb.js')
const proto = {};
proto.cogment = require('./agent_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.cogment.AgentClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.cogment.AgentPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.AgentStartRequest,
 *   !proto.cogment.AgentStartReply>}
 */
const methodInfo_Agent_Start = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.AgentStartReply,
  /** @param {!proto.cogment.AgentStartRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.AgentStartReply.deserializeBinary
);


/**
 * @param {!proto.cogment.AgentStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.AgentStartReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.AgentStartReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.AgentClient.prototype.start =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Agent/Start',
      request,
      metadata || {},
      methodInfo_Agent_Start,
      callback);
};


/**
 * @param {!proto.cogment.AgentStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.AgentStartReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.AgentPromiseClient.prototype.start =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Agent/Start',
      request,
      metadata || {},
      methodInfo_Agent_Start);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.AgentEndRequest,
 *   !proto.cogment.AgentEndReply>}
 */
const methodInfo_Agent_End = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.AgentEndReply,
  /** @param {!proto.cogment.AgentEndRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.AgentEndReply.deserializeBinary
);


/**
 * @param {!proto.cogment.AgentEndRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.AgentEndReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.AgentEndReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.AgentClient.prototype.end =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Agent/End',
      request,
      metadata || {},
      methodInfo_Agent_End,
      callback);
};


/**
 * @param {!proto.cogment.AgentEndRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.AgentEndReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.AgentPromiseClient.prototype.end =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Agent/End',
      request,
      metadata || {},
      methodInfo_Agent_End);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.AgentDecideRequest,
 *   !proto.cogment.AgentDecideReply>}
 */
const methodInfo_Agent_Decide = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.AgentDecideReply,
  /** @param {!proto.cogment.AgentDecideRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.AgentDecideReply.deserializeBinary
);


/**
 * @param {!proto.cogment.AgentDecideRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.AgentDecideReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.AgentDecideReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.AgentClient.prototype.decide =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Agent/Decide',
      request,
      metadata || {},
      methodInfo_Agent_Decide,
      callback);
};


/**
 * @param {!proto.cogment.AgentDecideRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.AgentDecideReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.AgentPromiseClient.prototype.decide =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Agent/Decide',
      request,
      metadata || {},
      methodInfo_Agent_Decide);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.AgentRewardRequest,
 *   !proto.cogment.AgentRewardReply>}
 */
const methodInfo_Agent_Reward = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.AgentRewardReply,
  /** @param {!proto.cogment.AgentRewardRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.AgentRewardReply.deserializeBinary
);


/**
 * @param {!proto.cogment.AgentRewardRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.AgentRewardReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.AgentRewardReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.AgentClient.prototype.reward =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Agent/Reward',
      request,
      metadata || {},
      methodInfo_Agent_Reward,
      callback);
};


/**
 * @param {!proto.cogment.AgentRewardRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.AgentRewardReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.AgentPromiseClient.prototype.reward =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Agent/Reward',
      request,
      metadata || {},
      methodInfo_Agent_Reward);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.VersionRequest,
 *   !proto.cogment.VersionInfo>}
 */
const methodInfo_Agent_Version = new grpc.web.AbstractClientBase.MethodInfo(
  cogment_api_common_pb.VersionInfo,
  /** @param {!proto.cogment.VersionRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  cogment_api_common_pb.VersionInfo.deserializeBinary
);


/**
 * @param {!proto.cogment.VersionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.VersionInfo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.VersionInfo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.AgentClient.prototype.version =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Agent/Version',
      request,
      metadata || {},
      methodInfo_Agent_Version,
      callback);
};


/**
 * @param {!proto.cogment.VersionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.VersionInfo>}
 *     A native promise that resolves to the response
 */
proto.cogment.AgentPromiseClient.prototype.version =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Agent/Version',
      request,
      metadata || {},
      methodInfo_Agent_Version);
};


module.exports = proto.cogment;

