/**
 * @fileoverview gRPC-Web generated client stub for cogment
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var cogment_api_common_pb = require('../../cogment/api/common_pb.js')
const proto = {};
proto.cogment = require('./hooks_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.cogment.TrialHooksClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.cogment.TrialHooksPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.TrialContext,
 *   !proto.cogment.TrialContext>}
 */
const methodInfo_TrialHooks_PreTrial = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.TrialContext,
  /** @param {!proto.cogment.TrialContext} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.TrialContext.deserializeBinary
);


/**
 * @param {!proto.cogment.TrialContext} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.TrialContext)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.TrialContext>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.TrialHooksClient.prototype.preTrial =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.TrialHooks/PreTrial',
      request,
      metadata || {},
      methodInfo_TrialHooks_PreTrial,
      callback);
};


/**
 * @param {!proto.cogment.TrialContext} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.TrialContext>}
 *     A native promise that resolves to the response
 */
proto.cogment.TrialHooksPromiseClient.prototype.preTrial =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.TrialHooks/PreTrial',
      request,
      metadata || {},
      methodInfo_TrialHooks_PreTrial);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.VersionRequest,
 *   !proto.cogment.VersionInfo>}
 */
const methodInfo_TrialHooks_Version = new grpc.web.AbstractClientBase.MethodInfo(
  cogment_api_common_pb.VersionInfo,
  /** @param {!proto.cogment.VersionRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  cogment_api_common_pb.VersionInfo.deserializeBinary
);


/**
 * @param {!proto.cogment.VersionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.VersionInfo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.VersionInfo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.TrialHooksClient.prototype.version =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.TrialHooks/Version',
      request,
      metadata || {},
      methodInfo_TrialHooks_Version,
      callback);
};


/**
 * @param {!proto.cogment.VersionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.VersionInfo>}
 *     A native promise that resolves to the response
 */
proto.cogment.TrialHooksPromiseClient.prototype.version =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.TrialHooks/Version',
      request,
      metadata || {},
      methodInfo_TrialHooks_Version);
};


module.exports = proto.cogment;

