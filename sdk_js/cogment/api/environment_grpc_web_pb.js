/**
 * @fileoverview gRPC-Web generated client stub for cogment
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var cogment_api_common_pb = require('../../cogment/api/common_pb.js')

var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js')
const proto = {};
proto.cogment = require('./environment_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.cogment.EnvironmentClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.cogment.EnvironmentPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.EnvStartRequest,
 *   !proto.cogment.EnvStartReply>}
 */
const methodInfo_Environment_Start = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.EnvStartReply,
  /** @param {!proto.cogment.EnvStartRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.EnvStartReply.deserializeBinary
);


/**
 * @param {!proto.cogment.EnvStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.EnvStartReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.EnvStartReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.EnvironmentClient.prototype.start =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Environment/Start',
      request,
      metadata || {},
      methodInfo_Environment_Start,
      callback);
};


/**
 * @param {!proto.cogment.EnvStartRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.EnvStartReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.EnvironmentPromiseClient.prototype.start =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Environment/Start',
      request,
      metadata || {},
      methodInfo_Environment_Start);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.EnvEndRequest,
 *   !proto.cogment.EnvEndReply>}
 */
const methodInfo_Environment_End = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.EnvEndReply,
  /** @param {!proto.cogment.EnvEndRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.EnvEndReply.deserializeBinary
);


/**
 * @param {!proto.cogment.EnvEndRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.EnvEndReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.EnvEndReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.EnvironmentClient.prototype.end =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Environment/End',
      request,
      metadata || {},
      methodInfo_Environment_End,
      callback);
};


/**
 * @param {!proto.cogment.EnvEndRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.EnvEndReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.EnvironmentPromiseClient.prototype.end =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Environment/End',
      request,
      metadata || {},
      methodInfo_Environment_End);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.EnvUpdateRequest,
 *   !proto.cogment.EnvUpdateReply>}
 */
const methodInfo_Environment_Update = new grpc.web.AbstractClientBase.MethodInfo(
  proto.cogment.EnvUpdateReply,
  /** @param {!proto.cogment.EnvUpdateRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.cogment.EnvUpdateReply.deserializeBinary
);


/**
 * @param {!proto.cogment.EnvUpdateRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.EnvUpdateReply)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.EnvUpdateReply>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.EnvironmentClient.prototype.update =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Environment/Update',
      request,
      metadata || {},
      methodInfo_Environment_Update,
      callback);
};


/**
 * @param {!proto.cogment.EnvUpdateRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.EnvUpdateReply>}
 *     A native promise that resolves to the response
 */
proto.cogment.EnvironmentPromiseClient.prototype.update =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Environment/Update',
      request,
      metadata || {},
      methodInfo_Environment_Update);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.cogment.VersionRequest,
 *   !proto.cogment.VersionInfo>}
 */
const methodInfo_Environment_Version = new grpc.web.AbstractClientBase.MethodInfo(
  cogment_api_common_pb.VersionInfo,
  /** @param {!proto.cogment.VersionRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  cogment_api_common_pb.VersionInfo.deserializeBinary
);


/**
 * @param {!proto.cogment.VersionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.cogment.VersionInfo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.cogment.VersionInfo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.cogment.EnvironmentClient.prototype.version =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/cogment.Environment/Version',
      request,
      metadata || {},
      methodInfo_Environment_Version,
      callback);
};


/**
 * @param {!proto.cogment.VersionRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.cogment.VersionInfo>}
 *     A native promise that resolves to the response
 */
proto.cogment.EnvironmentPromiseClient.prototype.version =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/cogment.Environment/Version',
      request,
      metadata || {},
      methodInfo_Environment_Version);
};


module.exports = proto.cogment;

