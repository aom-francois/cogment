apply_delta_replace = (observation, delta) => {
    return delta;
}

decode_observation_data = (actor_class, data, previous_observation) => {
  if(data.getSnapshot()) {
    return actor_class.observation_space.deserializeBinary(data.getContent());
  }
  else {
    delta = new actor_class.observation_delta.deserializeBinary(data.getContent());
    return actor_class.observation_delta_apply_fn(previous_observation, delta);
  }
}

module.exports.decode_observation_data = decode_observation_data;
module.exports.apply_delta_replace = apply_delta_replace;