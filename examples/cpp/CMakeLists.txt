add_subdirectory(common)

# Agents
add_subdirectory(agent/00_minimal)


# Environments
add_subdirectory(env/00_minimal)