#include "cogment/api/agent.egrpc.pb.h"

#include <grpcpp/grpcpp.h>
#include <spdlog/spdlog.h>


#include "easy_grpc/easy_grpc.h"
#include "easy_grpc_reflection/reflection.h"

namespace rpc = easy_grpc;

class Agent_service {
public:
    using service_type = cogment::Agent;
    
    // Called when a new trial is created.
    cogment::AgentStartReply Start(::cogment::AgentStartRequest) {
      return {};
    }

    cogment::AgentEndReply End(::cogment::AgentEndRequest) {
      return {};
    }

    cogment::AgentDecideReply Decide(::cogment::AgentDecideRequest) {
      return {};
    }


    cogment::AgentRewardReply Reward(::cogment::AgentRewardRequest r) {
      std::cout << "reward receivec" << r.DebugString() << "\n";
      return {};
    }

    cogment::VersionInfo Version(::cogment::VersionRequest) {
      return {};
    }
};

int main(int, const char** ) {
  rpc::Environment grpc_env;
  std::vector<rpc::Completion_queue> server_cqs(4);

  Agent_service service;

  rpc::server::Server server( rpc::server::Config()
    // 
    .add_default_listening_queues({server_cqs.begin(), server_cqs.end()})
    // Use our service
    .add_service(service)

    // Open an unsecured port
    .add_listening_port("0.0.0.0:9002")

    // enable reflection
    .add_feature(easy_grpc::Reflection_feature()));

  // Arrrrrrrrrrrg
  while(1) {
    std::this_thread::sleep_for(std::chrono::minutes(2));
  }

  return 0;
}