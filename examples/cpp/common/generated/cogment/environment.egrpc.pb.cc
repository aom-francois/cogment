// This code was generated by the easy_grpc protoc plugin.

#include "environment.egrpc.pb.h"

namespace aom_api {

// ********** Environment ********** //

const char* Environment::kEnvironment_Start_name = "/aom_api.Environment/Start";
const char* Environment::kEnvironment_Update_name = "/aom_api.Environment/Update";
const char* Environment::kEnvironment_Version_name = "/aom_api.Environment/Version";

Environment::Stub::Stub(::easy_grpc::client::Channel* c, ::easy_grpc::Completion_queue* default_queue)
  : channel_(c), default_queue_(default_queue ? default_queue : c->default_queue())
  , Start_tag_(c->register_method(kEnvironment_Start_name))
  , Update_tag_(c->register_method(kEnvironment_Update_name))
  , Version_tag_(c->register_method(kEnvironment_Version_name)) {}

// Start
::easy_grpc::Future<::aom_api::EnvStartReply> Environment::Stub::Start(::aom_api::EnvStartRequest req, ::easy_grpc::client::Call_options options) {
  if(!options.completion_queue) { options.completion_queue = default_queue_; }
  return ::easy_grpc::client::start_unary_call<::aom_api::EnvStartReply>(channel_, Start_tag_, std::move(req), std::move(options));
}

// Update
::easy_grpc::Future<::aom_api::EnvUpdateReply> Environment::Stub::Update(::aom_api::EnvUpdateRequest req, ::easy_grpc::client::Call_options options) {
  if(!options.completion_queue) { options.completion_queue = default_queue_; }
  return ::easy_grpc::client::start_unary_call<::aom_api::EnvUpdateReply>(channel_, Update_tag_, std::move(req), std::move(options));
}

// Version
::easy_grpc::Future<::aom_api::VersionInfo> Environment::Stub::Version(::aom_api::VersionRequest req, ::easy_grpc::client::Call_options options) {
  if(!options.completion_queue) { options.completion_queue = default_queue_; }
  return ::easy_grpc::client::start_unary_call<::aom_api::VersionInfo>(channel_, Version_tag_, std::move(req), std::move(options));
}

} // namespaceaom_api

