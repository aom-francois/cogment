cmake_minimum_required(VERSION 3.10)

project(cogment_framework VERSION 0.0.1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

enable_testing()

set(EASY_GRPC_BUILD_EXAMPLES OFF)
set(EASY_GRPC_BUILD_TESTS OFF)
set(Protobuf_USE_STATIC_LIBS ON)

add_subdirectory(easy_grpc)
add_subdirectory(orchestrator)
add_subdirectory(examples/cpp)
