stages:
  - prebuild
  - lint
  - test
  - build
  - deploy
  - release

variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2

# PREBUILD STAGES
docker_builder:
  stage: prebuild
  image: docker:18.09-dind
  services:
    - docker:dind
  variables:
    TAG: builder
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - apk add make
    - docker pull $CI_REGISTRY_IMAGE:builder
    - docker pull $CI_REGISTRY_IMAGE:cpp_builder
    - make cogment-builder
    - make cogment-cpp-builder
    - docker push $CI_REGISTRY_IMAGE:builder
    - docker push $CI_REGISTRY_IMAGE:cpp_builder
  only:
    - master

# LINT STAGES
cpp_lint:
  stage: lint
  image: docker:18.09-dind
  services:
    - docker:dind
  script:
    - apk add make git
    - make cpp_format
    - git diff --exit-code

python_lint:
  stage: lint
  image: python:3.7
  script:
    - pip install pycodestyle mypy
    - pycodestyle --exclude=*pb2.py,*pb2_grpc.py sdk_python/cogment/ log_exporter/
    - mypy --config-file sdk_python/mypy.ini sdk_python/cogment/
    - mypy --config-file log_exporter/mypy.ini log_exporter/

# orchestrator_chart_lint:
#   stage: lint
#   image: 
#     name: alpine/helm
#     entrypoint: [""]
#   script:
#     - helm lint charts/orchestrator/

cli_test:
  stage: test
  image: golang:1.13
  script: |
    apt-get update -y
    apt-get install -y protobuf-compiler
    cd cli
    go test -v ./...

# BUILD STAGES
orchestrator:
  stage: build
  image: docker:18.09-dind
  services:
    - docker:dind
  variables:
    IMAGE_NAME: orchestrator
  before_script:
    - apk add make
  artifacts:
    expire_in: 1 week
    paths:
      - _images
  script:
    - mkdir _images
    - make cogment-orchestrator
    - docker save $CI_REGISTRY_IMAGE/$IMAGE_NAME:latest > _images/orchestrator.tar
    - cat _images/orchestrator.tar | sha256sum -b > _images/orchestrator.hash

test_py_sdk:
  stage: build
  image: docker:18.09-dind
  services:
    - docker:dind
  before_script:
    - cd sdk_python
  script:
    - docker build -t sdk_py:latest .
    - docker run --rm sdk_py:latest python -m pytest

    # test_log_exporter_todo:
    #   stage: build
    #   image: docker:18.09-dind
    #   services:
    #     - docker:dind
    #   variables:
    #     IMAGE_NAME: log_exporter
    #   before_script:
    #     - cd log_exporter
    #   script:
    #     - docker build -t log_exporter:latest .
    # - docker run --rm log_exporter:latest python -m pytest


autodocs_sdk:
  stage: build
  image: docker:18.09-dind
  services:
    - docker:dind
  before_script:
    - apk add make
  artifacts:
    expire_in: 1 week
    paths:
      - docs/sdk
  script:
    - make docs_grpc

# DEPLOY STAGES
deploy_orchestrator:
  stage: deploy
  image: docker:18.09-dind
  services:
    - docker:dind
  variables:
    IMAGE_NAME: orchestrator
  before_script:
    - apk add bash
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - docker load -i _images/orchestrator.tar
    - ./orchestrator/deploy.sh $CI_REGISTRY_IMAGE/$IMAGE_NAME
  allow_failure: true
  only:
    - master

deploy_log_exporter:
  stage: deploy
  image: docker:18.09-dind
  services:
    - docker:dind
  variables:
    IMAGE_NAME: log_exporter
  before_script:
    - apk add bash
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - cd log_exporter
  script:
    - docker build -t $CI_REGISTRY_IMAGE/$IMAGE_NAME:latest .
    - docker run --rm $CI_REGISTRY_IMAGE/$IMAGE_NAME:latest python /exporter/version.py > version.txt
    - docker tag $CI_REGISTRY_IMAGE/$IMAGE_NAME:latest $CI_REGISTRY_IMAGE/$IMAGE_NAME:$(cat version.txt)
    - docker push $CI_REGISTRY_IMAGE/$IMAGE_NAME:latest
    - docker push $CI_REGISTRY_IMAGE/$IMAGE_NAME:$(cat version.txt)
  only:
    - master


#deploy_chart_orchestrator:
#  stage: deploy
#  image: docker:18.09-dind
#  services:
#    - docker:dind
#  before_script:
#    - apk add bash curl openssl jq
#    - curl https://raw.githubusercontent.com/helm/helm/master/scripts/get > get_helm.sh
#    - chmod +x get_helm.sh
#    - ./get_helm.sh
#    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
#  script:
#    - helm init --client-only
#    - helm package charts/orchestrator/
#
#    - curl --request DELETE --header "Private-Token:${CI_ACCESS_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/$CI_COMMIT_TAG"
#    - FILENAME=$(ls | grep orchestrator*.tgz)
#    - URL=$(curl --request POST --header "Private-Token:${CI_ACCESS_TOKEN}" --form "file=@${FILENAME}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/uploads" | jq --raw-output '.url')
#
#    - curl --request POST --header "Content-Type:application/json" --header "Private-Token:${CI_ACCESS_TOKEN}"
#      --data "{
#      \"name\":\"Chart Release\",
#      \"tag_name\":\"${CI_COMMIT_TAG}\",
#      \"description\":\"Release of the orchestrator chart\",
#      \"assets\":{
#      \"links\":[{
#      \"name\":\"chart-${FILENAME}\",
#      \"url\":\"${CI_PROJECT_URL}${URL}\"
#      }]
#      }
#      }"
#      ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases
#  only:
#    - tags
#
#js_sdk:
#  stage: deploy
#  image: node:10
#  script:
#    - cd sdk_js
#    - npm install
#    - echo "//registry.npmjs.org/:_authToken=$NPM_AUTH_TOKEN" > ~/.npmrc
#    - npm publish
#  allow_failure: true
#  only:
#    - master

python_sdk:
  stage: deploy
  image: python:3.6-alpine
  variables:
    TWINE_USERNAME: $PYPI_USERNAME
    TWINE_PASSWORD: $PYPI_PASSWORD
  script:
    - pip install twine
    - cd sdk_python
    - python setup.py sdist
    - twine upload dist/*
  allow_failure: true
  only:
    - master

pages:
  stage: deploy
  image:
    name: squidfunk/mkdocs-material
    entrypoint: [""]
  script:
    - mkdocs build
  artifacts:
    paths:
      - public
  only:
    - master

cli_release:
  stage: release
  image: golang:1.13
  script: |
    apt-get update -y
    apt-get install -y jq

    cd cli

    RELEASE_TAG=${CI_COMMIT_TAG:-latest}

    CLI_VERSION=${RELEASE_TAG} make release

    LINUX_BUILD_FILENAME="cogment-linux-amd64"
    MACOS_BUILD_FILENAME="cogment-macOS-amd64"
    WINDOWS_BUILD_FILENAME="cogment-windows-amd64.exe"

    curl --request DELETE --header "Private-Token:${CI_ACCESS_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/${RELEASE_TAG}"

    LINUX_BUILD_URL=$(curl --request POST --header "Private-Token:${CI_ACCESS_TOKEN}" --form "file=@build/${LINUX_BUILD_FILENAME}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/uploads" | jq --raw-output '.url')
    MACOS_BUILD_URL=$(curl --request POST --header "Private-Token:${CI_ACCESS_TOKEN}" --form "file=@build/${MACOS_BUILD_FILENAME}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/uploads" | jq --raw-output '.url')
    WINDOWS_BUILD_URL=$(curl --request POST --header "Private-Token:${CI_ACCESS_TOKEN}" --form "file=@build/${WINDOWS_BUILD_FILENAME}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/uploads" | jq --raw-output '.url')

    curl --request POST --header "Content-Type:application/json" --header "Private-Token:${CI_ACCESS_TOKEN}" \
              --data "{
                  \"name\":\"Cogment CLI\",
                  \"tag_name\":\"${RELEASE_TAG}\",
                  \"ref\":\"${CI_COMMIT_SHA}\",
                  \"description\":\"Cogment CLI release\",
                  \"assets\":{
                      \"links\":[{
                          \"name\":\"${LINUX_BUILD_FILENAME}\",
                          \"url\":\"${CI_PROJECT_URL}${LINUX_BUILD_URL}\"
                      },{
                          \"name\":\"${MACOS_BUILD_FILENAME}\",
                          \"url\":\"${CI_PROJECT_URL}${MACOS_BUILD_URL}\"
                      },{
                          \"name\":\"${WINDOWS_BUILD_FILENAME}\",
                          \"url\":\"${CI_PROJECT_URL}${WINDOWS_BUILD_URL}\"
                      }]
                    }
                }" \
              ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases

  only:
    - tags