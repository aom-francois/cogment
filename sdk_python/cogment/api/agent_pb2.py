# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: cogment/api/agent.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from cogment.api import common_pb2 as cogment_dot_api_dot_common__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='cogment/api/agent.proto',
  package='cogment',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x17\x63ogment/api/agent.proto\x12\x07\x63ogment\x1a\x18\x63ogment/api/common.proto\"s\n\x11\x41gentStartRequest\x12\x10\n\x08trial_id\x18\x01 \x01(\t\x12\x10\n\x08\x61\x63tor_id\x18\x02 \x01(\x05\x12$\n\x06\x63onfig\x18\x03 \x01(\x0b\x32\x14.cogment.ActorConfig\x12\x14\n\x0c\x61\x63tor_counts\x18\x04 \x03(\x05\"\x11\n\x0f\x41gentStartReply\"5\n\x0f\x41gentEndRequest\x12\x10\n\x08trial_id\x18\x01 \x01(\t\x12\x10\n\x08\x61\x63tor_id\x18\x02 \x01(\x05\"\x0f\n\rAgentEndReply\"c\n\x12\x41gentDecideRequest\x12\x10\n\x08trial_id\x18\x01 \x01(\t\x12\x10\n\x08\x61\x63tor_id\x18\x02 \x01(\x05\x12)\n\x0bobservation\x18\x03 \x01(\x0b\x32\x14.cogment.Observation\"Y\n\x10\x41gentDecideReply\x12\x1f\n\x06\x61\x63tion\x18\x01 \x01(\x0b\x32\x0f.cogment.Action\x12$\n\tfeedbacks\x18\x02 \x03(\x0b\x32\x11.cogment.Feedback\"Q\n\x06Reward\x12\r\n\x05value\x18\x01 \x01(\x02\x12\x12\n\nconfidence\x18\x02 \x01(\x02\x12$\n\tfeedbacks\x18\x03 \x03(\x0b\x32\x11.cogment.Feedback\"j\n\x12\x41gentRewardRequest\x12\x10\n\x08trial_id\x18\x01 \x01(\t\x12\x10\n\x08\x61\x63tor_id\x18\x02 \x01(\x05\x12\x0f\n\x07tick_id\x18\x04 \x01(\x05\x12\x1f\n\x06reward\x18\x03 \x01(\x0b\x32\x0f.cogment.Reward\"\x12\n\x10\x41gentRewardReply2\xc7\x02\n\x05\x41gent\x12?\n\x05Start\x12\x1a.cogment.AgentStartRequest\x1a\x18.cogment.AgentStartReply\"\x00\x12\x39\n\x03\x45nd\x12\x18.cogment.AgentEndRequest\x1a\x16.cogment.AgentEndReply\"\x00\x12\x42\n\x06\x44\x65\x63ide\x12\x1b.cogment.AgentDecideRequest\x1a\x19.cogment.AgentDecideReply\"\x00\x12\x42\n\x06Reward\x12\x1b.cogment.AgentRewardRequest\x1a\x19.cogment.AgentRewardReply\"\x00\x12:\n\x07Version\x12\x17.cogment.VersionRequest\x1a\x14.cogment.VersionInfo\"\x00\x62\x06proto3')
  ,
  dependencies=[cogment_dot_api_dot_common__pb2.DESCRIPTOR,])




_AGENTSTARTREQUEST = _descriptor.Descriptor(
  name='AgentStartRequest',
  full_name='cogment.AgentStartRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trial_id', full_name='cogment.AgentStartRequest.trial_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='actor_id', full_name='cogment.AgentStartRequest.actor_id', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='config', full_name='cogment.AgentStartRequest.config', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='actor_counts', full_name='cogment.AgentStartRequest.actor_counts', index=3,
      number=4, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=62,
  serialized_end=177,
)


_AGENTSTARTREPLY = _descriptor.Descriptor(
  name='AgentStartReply',
  full_name='cogment.AgentStartReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=179,
  serialized_end=196,
)


_AGENTENDREQUEST = _descriptor.Descriptor(
  name='AgentEndRequest',
  full_name='cogment.AgentEndRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trial_id', full_name='cogment.AgentEndRequest.trial_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='actor_id', full_name='cogment.AgentEndRequest.actor_id', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=198,
  serialized_end=251,
)


_AGENTENDREPLY = _descriptor.Descriptor(
  name='AgentEndReply',
  full_name='cogment.AgentEndReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=253,
  serialized_end=268,
)


_AGENTDECIDEREQUEST = _descriptor.Descriptor(
  name='AgentDecideRequest',
  full_name='cogment.AgentDecideRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trial_id', full_name='cogment.AgentDecideRequest.trial_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='actor_id', full_name='cogment.AgentDecideRequest.actor_id', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='observation', full_name='cogment.AgentDecideRequest.observation', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=270,
  serialized_end=369,
)


_AGENTDECIDEREPLY = _descriptor.Descriptor(
  name='AgentDecideReply',
  full_name='cogment.AgentDecideReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='action', full_name='cogment.AgentDecideReply.action', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='feedbacks', full_name='cogment.AgentDecideReply.feedbacks', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=371,
  serialized_end=460,
)


_REWARD = _descriptor.Descriptor(
  name='Reward',
  full_name='cogment.Reward',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='value', full_name='cogment.Reward.value', index=0,
      number=1, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='confidence', full_name='cogment.Reward.confidence', index=1,
      number=2, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='feedbacks', full_name='cogment.Reward.feedbacks', index=2,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=462,
  serialized_end=543,
)


_AGENTREWARDREQUEST = _descriptor.Descriptor(
  name='AgentRewardRequest',
  full_name='cogment.AgentRewardRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trial_id', full_name='cogment.AgentRewardRequest.trial_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='actor_id', full_name='cogment.AgentRewardRequest.actor_id', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='tick_id', full_name='cogment.AgentRewardRequest.tick_id', index=2,
      number=4, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='reward', full_name='cogment.AgentRewardRequest.reward', index=3,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=545,
  serialized_end=651,
)


_AGENTREWARDREPLY = _descriptor.Descriptor(
  name='AgentRewardReply',
  full_name='cogment.AgentRewardReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=653,
  serialized_end=671,
)

_AGENTSTARTREQUEST.fields_by_name['config'].message_type = cogment_dot_api_dot_common__pb2._ACTORCONFIG
_AGENTDECIDEREQUEST.fields_by_name['observation'].message_type = cogment_dot_api_dot_common__pb2._OBSERVATION
_AGENTDECIDEREPLY.fields_by_name['action'].message_type = cogment_dot_api_dot_common__pb2._ACTION
_AGENTDECIDEREPLY.fields_by_name['feedbacks'].message_type = cogment_dot_api_dot_common__pb2._FEEDBACK
_REWARD.fields_by_name['feedbacks'].message_type = cogment_dot_api_dot_common__pb2._FEEDBACK
_AGENTREWARDREQUEST.fields_by_name['reward'].message_type = _REWARD
DESCRIPTOR.message_types_by_name['AgentStartRequest'] = _AGENTSTARTREQUEST
DESCRIPTOR.message_types_by_name['AgentStartReply'] = _AGENTSTARTREPLY
DESCRIPTOR.message_types_by_name['AgentEndRequest'] = _AGENTENDREQUEST
DESCRIPTOR.message_types_by_name['AgentEndReply'] = _AGENTENDREPLY
DESCRIPTOR.message_types_by_name['AgentDecideRequest'] = _AGENTDECIDEREQUEST
DESCRIPTOR.message_types_by_name['AgentDecideReply'] = _AGENTDECIDEREPLY
DESCRIPTOR.message_types_by_name['Reward'] = _REWARD
DESCRIPTOR.message_types_by_name['AgentRewardRequest'] = _AGENTREWARDREQUEST
DESCRIPTOR.message_types_by_name['AgentRewardReply'] = _AGENTREWARDREPLY
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

AgentStartRequest = _reflection.GeneratedProtocolMessageType('AgentStartRequest', (_message.Message,), dict(
  DESCRIPTOR = _AGENTSTARTREQUEST,
  __module__ = 'cogment.api.agent_pb2'
  # @@protoc_insertion_point(class_scope:cogment.AgentStartRequest)
  ))
_sym_db.RegisterMessage(AgentStartRequest)

AgentStartReply = _reflection.GeneratedProtocolMessageType('AgentStartReply', (_message.Message,), dict(
  DESCRIPTOR = _AGENTSTARTREPLY,
  __module__ = 'cogment.api.agent_pb2'
  # @@protoc_insertion_point(class_scope:cogment.AgentStartReply)
  ))
_sym_db.RegisterMessage(AgentStartReply)

AgentEndRequest = _reflection.GeneratedProtocolMessageType('AgentEndRequest', (_message.Message,), dict(
  DESCRIPTOR = _AGENTENDREQUEST,
  __module__ = 'cogment.api.agent_pb2'
  # @@protoc_insertion_point(class_scope:cogment.AgentEndRequest)
  ))
_sym_db.RegisterMessage(AgentEndRequest)

AgentEndReply = _reflection.GeneratedProtocolMessageType('AgentEndReply', (_message.Message,), dict(
  DESCRIPTOR = _AGENTENDREPLY,
  __module__ = 'cogment.api.agent_pb2'
  # @@protoc_insertion_point(class_scope:cogment.AgentEndReply)
  ))
_sym_db.RegisterMessage(AgentEndReply)

AgentDecideRequest = _reflection.GeneratedProtocolMessageType('AgentDecideRequest', (_message.Message,), dict(
  DESCRIPTOR = _AGENTDECIDEREQUEST,
  __module__ = 'cogment.api.agent_pb2'
  # @@protoc_insertion_point(class_scope:cogment.AgentDecideRequest)
  ))
_sym_db.RegisterMessage(AgentDecideRequest)

AgentDecideReply = _reflection.GeneratedProtocolMessageType('AgentDecideReply', (_message.Message,), dict(
  DESCRIPTOR = _AGENTDECIDEREPLY,
  __module__ = 'cogment.api.agent_pb2'
  # @@protoc_insertion_point(class_scope:cogment.AgentDecideReply)
  ))
_sym_db.RegisterMessage(AgentDecideReply)

Reward = _reflection.GeneratedProtocolMessageType('Reward', (_message.Message,), dict(
  DESCRIPTOR = _REWARD,
  __module__ = 'cogment.api.agent_pb2'
  # @@protoc_insertion_point(class_scope:cogment.Reward)
  ))
_sym_db.RegisterMessage(Reward)

AgentRewardRequest = _reflection.GeneratedProtocolMessageType('AgentRewardRequest', (_message.Message,), dict(
  DESCRIPTOR = _AGENTREWARDREQUEST,
  __module__ = 'cogment.api.agent_pb2'
  # @@protoc_insertion_point(class_scope:cogment.AgentRewardRequest)
  ))
_sym_db.RegisterMessage(AgentRewardRequest)

AgentRewardReply = _reflection.GeneratedProtocolMessageType('AgentRewardReply', (_message.Message,), dict(
  DESCRIPTOR = _AGENTREWARDREPLY,
  __module__ = 'cogment.api.agent_pb2'
  # @@protoc_insertion_point(class_scope:cogment.AgentRewardReply)
  ))
_sym_db.RegisterMessage(AgentRewardReply)



_AGENT = _descriptor.ServiceDescriptor(
  name='Agent',
  full_name='cogment.Agent',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=674,
  serialized_end=1001,
  methods=[
  _descriptor.MethodDescriptor(
    name='Start',
    full_name='cogment.Agent.Start',
    index=0,
    containing_service=None,
    input_type=_AGENTSTARTREQUEST,
    output_type=_AGENTSTARTREPLY,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='End',
    full_name='cogment.Agent.End',
    index=1,
    containing_service=None,
    input_type=_AGENTENDREQUEST,
    output_type=_AGENTENDREPLY,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='Decide',
    full_name='cogment.Agent.Decide',
    index=2,
    containing_service=None,
    input_type=_AGENTDECIDEREQUEST,
    output_type=_AGENTDECIDEREPLY,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='Reward',
    full_name='cogment.Agent.Reward',
    index=3,
    containing_service=None,
    input_type=_AGENTREWARDREQUEST,
    output_type=_AGENTREWARDREPLY,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='Version',
    full_name='cogment.Agent.Version',
    index=4,
    containing_service=None,
    input_type=cogment_dot_api_dot_common__pb2._VERSIONREQUEST,
    output_type=cogment_dot_api_dot_common__pb2._VERSIONINFO,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_AGENT)

DESCRIPTOR.services_by_name['Agent'] = _AGENT

# @@protoc_insertion_point(module_scope)
