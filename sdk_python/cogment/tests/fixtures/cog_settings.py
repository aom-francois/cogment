import cogment as _cog
from types import SimpleNamespace

import fixtures.rps_pb2
import fixtures.rps_delta


_player_class = _cog.ActorClass(
    id='player',
    config_type=None,
    action_space=fixtures.rps_pb2.ActorAction,
    observation_space=fixtures.rps_pb2.GameState,
    observation_delta=fixtures.rps_pb2.GameStateDelta,
    observation_delta_apply_fn=fixtures.rps_delta.apply_delta_gs,
    feedback_space=None
)

_judge_class = _cog.ActorClass(
    id='judge',
    config_type=None,
    action_space=fixtures.rps_pb2.JudgeAction,
    observation_space=fixtures.rps_pb2.JudgeView,
    observation_delta=fixtures.rps_pb2.JudgeView,
    observation_delta_apply_fn=_cog.delta_encoding._apply_delta_replace,
    feedback_space=None
)


actor_classes = _cog.actor_class.ActorClassList(
    _player_class,
    _judge_class,
)


environment = SimpleNamespace(
    config_type=None,
    actors=[
        (_player_class, 3),
        (_judge_class, 1),
    ]
)
